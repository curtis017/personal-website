function onclickHandler() {

  // Create Spinner Icon
  var spinner = document.createElement("i");
  spinner.className = 'fa fa-circle-o-notch fa-spin';
  spinner.setAttribute("id", "spinner");

  // Give user immediate feedback by replacing the submit text with spinner
  document.getElementById("submit").innerHTML = '';
  document.getElementById("submit").appendChild(spinner);

  // Initialize variables for request
  var xhr = new XMLHttpRequest();
  var name = document.getElementById('name').value;
  var email = document.getElementById('email').value;
  var body = document.getElementById('body').value;

  xhr.open('POST', 'https://h0l175wszk.execute-api.us-east-1.amazonaws.com/production/');
  xhr.setRequestHeader('Content-Type', 'application/json', true);
  xhr.setRequestHeader('x-api-key', '*****'); // Client side key that is rate limited

  // Execute when the response returns
  xhr.onload = function() {
    console.log(JSON.parse(xhr.responseText));
    document.getElementById('name').value = '';
    document.getElementById('email').value = '';
    document.getElementById('body').value = '';
    document.getElementById('name').disabled = true;
    document.getElementById('email').disabled = true;
    document.getElementById('body').disabled = true;
    document.getElementById('spinner').remove();
    document.getElementById("submit").className += ' pure-button-disabled';
    document.getElementById("submit").innerHTML = 'Thank you!';
  };

  // Execute on error
  xhr.onerror = function(err) {
    console.log(err);
    document.getElementById('spinner').remove();
    document.getElementById("submit").className += ' button-error pure-button-disabled';
    document.getElementById("submit").innerHTML = 'Whoops! Please try again.';
    setTimeout(function()
    {
      document.getElementById("submit").className = 'pure-button';
      document.getElementById("submit").innerHTML = 'Submit';
    }, 3000);
  }

  // Send the request
  xhr.send(JSON.stringify({
    "name": name,
    "email": email,
    "body": body
  }));
}

function inputValidation() {
  var valid = ((document.getElementById('name').value != '') && (document.getElementById('email').value != '') && (document.getElementById('body').value != ''));
  document.getElementById('submit').className = (valid && (!document.querySelectorAll(':invalid').length)) ? 'pure-button' : 'pure-button pure-button-disabled';
}
