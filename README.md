# My Personal Website

#### Installation
After downloading the project you will need to install the dependencies. This is easy with the npm package manager. The following command uses *package.json* to install the correct dependencies and their corresponding versions. 

`$ npm install`

#### Running locally
The following command will build and run the application on your local computer.

`$ npm start`

#### Build
The following command will create a full build of the repository. All of the code will be placed inside *./dist*:

`$ npm run build`

#### Deploy to AWS
The following command will build and push the entire repository to an s3 bucket, which is then hosted by CloudFront. It uses environment variables for permissions (read more [here](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html)).

`$ npm run deploy`
